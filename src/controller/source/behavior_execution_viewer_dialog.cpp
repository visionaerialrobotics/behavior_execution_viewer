/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/behavior_execution_viewer_dialog.h"


#include "../include/global.h"

BehaviorExecutionViewerDialog::BehaviorExecutionViewerDialog(QWidget* parent, int caso, QTableWidgetItem* clicked_item
   , std::string task_name,TreeFileManager* tfm_p): QDialog(parent), ui(new Ui::BehaviorExecutionViewerDialog)
{
        std::cout << "file behavior_execution_viewer_dialog, function constructor"<< std::endl;

  ui->setupUi(this);


  this->parent = parent;
  this->caso = caso;
  this->clicked_item = clicked_item;
  this->task_name=task_name;

  // Nodes
  n.param<std::string>("robot_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("stop_task", stop_task, "stop_task");
  n.param<std::string>("start_task", start_task, "start_task");

  start_task_srv=n.serviceClient<behavior_coordinator_msgs::StartTask>("/"+drone_id_namespace+"/"+start_task);
  stop_task_srv = n.serviceClient<behavior_coordinator_msgs::StopTask>("/" + drone_id_namespace + "/" + stop_task);

  tfm=tfm_p;



  switch (this->caso)
  {
    case 0:
      // Add behavior
      {

        // Connects
        connect(ui->button_accept, SIGNAL(clicked()), this, SLOT(action_accept()));
        connect(ui->button_cancel, SIGNAL(clicked()), this, SLOT(action_cancel()));


        this->setUpBehaviorCombobox();
        this->setWindowTitle(QString::fromStdString("Start a task"));

        ui->widget_2->init(tfm_p);

        //change text content if we have selected another task
        connect(ui->behavior_combobox,SIGNAL(currentIndexChanged(const QString &)),ui->widget_2,SLOT(controlWidget(const QString &)));

        break;
      }
    case 2:
      // Stop behavior
      {
        this->action_accept();
        break;
      }

  }
}

BehaviorExecutionViewerDialog::~BehaviorExecutionViewerDialog()
{
}



void BehaviorExecutionViewerDialog::setUpBehaviorCombobox()
{
   std::cout << "file behavior_execution_viewer_dialog, function setUpBehaviorCombobox"<< std::endl;



  std::vector<std::string> available_behaviors = tfm->getBehaviors();

  QList<QString>* available_behaviors_list = new QList<QString>();
  for (int i = 0; i < available_behaviors.size(); i++)
  {
    std::string behavior_aux = available_behaviors[i];
    available_behaviors_list->append(QString::fromStdString(behavior_aux));
  }


  QStringList definitive_list = *available_behaviors_list;
  ui->behavior_combobox->addItems(definitive_list);
}

void BehaviorExecutionViewerDialog::action_accept()
{
     std::cout << "file behavior_execution_viewer_dialog, function action_accept"<< std::endl;


  bool correct = false;
  switch (this->caso)
  {
    case 0:
    {
      // Add behavior

      if (!checkBehaviorArguments())
        break;


      behavior_coordinator_msgs::StartTask::Response res_start;
      behavior_coordinator_msgs::StartTask::Request req_start;

      behavior_coordinator_msgs::TaskCommand task;
      task.name= ui->behavior_combobox->currentText().toUtf8().constData();
      task.parameters=ui->widget_2->getText();
      task.priority=3;
      req_start.task = task;
      if(start_task_srv.call(req_start,res_start))
      {
          if((bool)res_start.ack)
          {
              std::cout << "TASK STARTED" << std::endl ;
              correct=true;
          }
          else
          {


              // error_message.setTextFormat(Qt::RichText);
               QString richtext( "Task "+QString::fromStdString(task.name)+" is unable to start");
               QString & txt= richtext;
               display_Warning("Warning",txt);
          }
      }
      else
      {
          ROS_ERROR("Call to server failed, because of reasons.");
      }

      break;
    }
    case 2:
    {
      // Stop behavior

      bool ok;
      std::string error;
      behavior_coordinator_msgs::StopTask msg;
      msg.request.name=task_name;

      if(stop_task_srv.call(msg))
      {
          if((bool)msg.response.ack )
          {
              std::cout << "THE TASK WAS STOPPED" << std::endl ;
          }
          else
          {

              QString richtext2(QString::fromStdString("Error"));
              QString & tittle= richtext2;
              error=msg.response.error_message;
              QString richtext (QString::fromStdString(" Task " + task_name + " didnt stop "+ error));
              QString & txt= richtext;
              display_Warning(tittle,txt);
          }
      }
      else{
           ROS_ERROR("Call to server failed, because of reasons.");
      }



      correct = true;
      break;
    }
  }

  if (correct)
    this->close();
}

void BehaviorExecutionViewerDialog::action_cancel()
{
  this->close();
}



bool BehaviorExecutionViewerDialog::checkBehaviorArguments()
{
       std::cout << "file behavior_execution_viewer_dialog, function checkBehaviorArguments"<< std::endl;

  QMessageBox error_message;


  std::string parameter=ui->widget_2->getText();
  //std::cout << "este es el parametro usado "<<paramet <<" y este el task"<<ui->behavior_combobox->currentText().toUtf8().constData()<<std::endl;
  bool res;
  if (!parameter.size()==0)
  {
     res = tfm->checkParameters(ui->behavior_combobox->currentText().toUtf8().constData(),parameter);
  }
  else
  {
    
    res = true;
  }

  if (!res)
  {
    error_message.setWindowTitle(QString::fromStdString("Invalid task parameters"));
    error_message.setText(QString::fromStdString("The input doesn't match expected values"));
    error_message.exec();
    return false;
  }
  else{
      return true;

  }
}




void BehaviorExecutionViewerDialog::display_Warning(const QString & tittle,const QString & txt )
{
    QDialog dlg;
    auto layout = new QVBoxLayout();
    layout->addWidget(new QLabel(txt));

    auto h_layout = new QHBoxLayout();
    h_layout->addStretch();
    auto ok_button = new QPushButton("OK");
    layout->addLayout(h_layout);
    dlg.setLayout(layout);
    dlg.layout()->setSizeConstraint( QLayout::SetFixedSize );
    dlg.setWindowTitle(tittle);
    dlg.exec() ;
}

