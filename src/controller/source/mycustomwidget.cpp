#include "mycustomwidget.h"

#include <QPushButton>
#include <QHBoxLayout>
#include <iostream>
#include <QLabel>
#include<QLayoutItem>
#include <QSize>
#include <cstdlib>
MyCustomWidget::MyCustomWidget(QWidget *parent)
    : QWidget(parent)
{


    first_iter=true;
    tfm=0;
    std::cout <<"MyCustomWidget constructor" << std::endl;

    top_line = new QFrame;
    top_line->setFrameShape(QFrame::HLine);
    top_line->setFrameShadow(QFrame::Sunken);

    bot_line = new QFrame;
    bot_line->setFrameShape(QFrame::HLine);
    bot_line->setFrameShadow(QFrame::Sunken);

    vertical_layout= new QVBoxLayout(this);

    grid= new QGridLayout();

    top_line_layout= new QHBoxLayout;

    bot_line_layout= new QHBoxLayout;

    top_line_layout->addWidget(top_line);

    bot_line_layout->addWidget(bot_line);

    vertical_layout->addLayout(top_line_layout);

    vertical_layout->addLayout(grid);

    vertical_layout->addLayout(bot_line_layout);

    grid->setContentsMargins(-1, 0, -1, 0);

    top_line_layout->setContentsMargins(-1, -1, -1, 0);

    top_line_layout->setContentsMargins(-1, 0, -1, -1);

    vertical_layout->setSpacing(20);

    grid->setVerticalSpacing(6);

    bot_line->hide();
}


MyCustomWidget::~MyCustomWidget()
{
}

bool MyCustomWidget::addWidgets(const std::vector<std::string> & names,const std::vector<std::string> & params )
{

    QLabel * param_name= nullptr;
    QLabel * param_example=nullptr;
    QLineEdit * parameters=nullptr;

    int cont=1;
    bool res=true;
    for(int i=0 ; i < names.size() ;i++)
    {

        param_name = new QLabel(QString::fromStdString(names[i]+":"));
        parameters= new QLineEdit();
        //parameters->resize(parameters->maximumSize());
        param_example=new QLabel(QString::fromStdString(params[i]));
        param_example->setAlignment(Qt::AlignTop);
        lineEdit_toLabel.insert(std::pair<QLineEdit *,std::string>(parameters,names[i]));
        grid->addWidget(param_name,cont,0);
        grid->addWidget(parameters,cont,1,0);
        grid->addWidget(param_example,cont+1,1);
        cont+=2;
        res=false;
    }

    if(cont==3)
    {
        //there is one parama
        QLabel * top_label= new QLabel("Parameter:");
        //QHBoxLayout * top_layout= new QHBoxLayout();
        //top_layout->addWidget(top_label);
        //top_layout->setContentsMargins(75,0,0,0);
        //grid->addLayout(top_layout,0,1);
        top_label->setAlignment(Qt::AlignHCenter);
        grid->addWidget(top_label,0,1);

    }
    else if(cont > 3 )
    {
        //there are more than one paramterer
        QLabel * top_label= new QLabel("Parameters:");
        //QHBoxLayout * top_layout= new QHBoxLayout();
        //top_layout->setAlignment(Qt::AlignHCenter);
        //top_layout->addWidget(top_label);
        //top_layout->setContentsMargins(75,0,0,0);
        //grid->addLayout(top_layout,0,1);
        grid->addWidget(top_label,0,1,Qt::AlignHCenter);
    }

    return res;



}
void MyCustomWidget::deleteWidgets(QLayout* layout)
{
    std::cout << "deleteWidgets" << std::endl;
    QLayoutItem* child;
    while ( layout->count() != 0 ) {
        child = layout->takeAt ( 0 );
        if ( child->layout() != 0 ) {
            deleteWidgets( child->layout() );
        } else if ( child->widget() != 0 ) {
            delete child->widget();
        }

        delete child;
    }

}

void MyCustomWidget::controlWidget(const QString & new_task)
{


    deleteWidgets(grid);
    lineEdit_toLabel.clear();


    std::vector<std::string> names;
    std::vector<std::string> params;
    if(tfm!=0)
    {

        std::map<std::string,std::vector<std::string>> task_param=tfm->getBehaviors_loaded_complete().at(new_task.toStdString());
        for (const auto &p: task_param)
        {
            names.push_back(p.first);
            params.push_back(procces_params(p.second));
        }
    }



    bool hide_widget=addWidgets(names,params);

    updateGeometry();

    if(hide_widget)
    {
        bot_line->hide();
    }
    else
    {
        top_line->show();
        bot_line->show();
    }

    if(new_task == "ROTATE")
        rotate_special_function();


    first_iter=false;


}

std::string MyCustomWidget::getText()
{
    std::cout << "getText" << std::endl;
    for ( const auto &p : lineEdit_toLabel )
    {

        std::string content =p.first->text().toStdString();
        if (content != "")
            text+=p.second+": "+content + "\n";
    }
    std::string res= text;
    text="";
    return res;
}


void MyCustomWidget::init(TreeFileManager * ref_tfm)
{
  tfm=ref_tfm;
}


std::string MyCustomWidget::procces_params(std::vector<std::string> params)
{
    std::string res="";
    if(params.size()==1)
    {

        //multiple choices for the type(point, point_listt, string, list_of_strings)

        if(params[0].compare("point")==0)
            res.append("Point e.g: [0,0,3]");

        else if (params[0].compare("point_list")==0)
            res.append(" List of points e.g: [[0,0,3],[1,0,3]]");
        else if (params[0].compare("string")==0)
            res.append("String e.g: 'text'");
        else if (params[0].compare("list_of_strings")==0)
            res.append("List of Strings e.g: ['text a', 'text b']");

    }
    else
    {
        //check if is an array of numbers or enumerate
        // e.g: [0,30] or [FORWARD,BACKWARD]
        char * end;

        int a;

        a=  strtol(params[0].c_str(), &end, 10);
        if(end == params[0])
        {
            //FORWARD
            res.append(" Allowed values: ");
            for (int i=0;i < params.size();i++)
            {
                res.append(params[i]);
                res.append(",");
            }
            res.erase(res.size()-1);
        }
        else
        {
            // Number
            res.append("Number between : ");
            res.append("[");
            for (int i=0;i < params.size();i++)
            {
                res.append(params[i]);
                res.append(",");
            }
            res.erase(res.size()-1);
            res.append("]");
        }
    }

    return res;
}



void MyCustomWidget::rotate_special_function()
{

    std::vector<QLineEdit *> lines;
    for ( const auto &p : lineEdit_toLabel )
    {

        lines.push_back(p.first);
    }
    connect(lines[0], SIGNAL(textEdited(const QString &)), this, SLOT(rotate_callback_1()));
    connect(lines[1],SIGNAL(textEdited(const QString &)), this, SLOT(rotate_callback_2()));

}

void MyCustomWidget::rotate_callback_1()
{
    std::vector<QLineEdit *> lines;
    for ( const auto &p : lineEdit_toLabel )
    {
        lines.push_back(p.first);
    }
    if(lines[0]->text().size()==0)
        lines[1]->setReadOnly(false);
    else
        lines[1]->setReadOnly(true);

}
void MyCustomWidget::rotate_callback_2()
{
    std::vector<QLineEdit *> lines;
    for ( const auto &p : lineEdit_toLabel )
    {
        lines.push_back(p.first);
    }

    if(lines[1]->text().size()==0)
        lines[0]->setReadOnly(false);
    else
        lines[0]->setReadOnly(true);
}
