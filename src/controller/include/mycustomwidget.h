#ifndef MyCustomWidget_H
#define MyCustomWidget_H

#include <QWidget>
#include <QGridLayout>
#include <map>
#include <QLineEdit>
#include <vector>
#include "tree_file_manager.h"
#include <QVBoxLayout>
#include <QFrame>
#include <QHBoxLayout>

class MyCustomWidget : public QWidget
{
    Q_OBJECT

public:
    MyCustomWidget(QWidget *parent = nullptr);
    ~MyCustomWidget();

    void init(TreeFileManager * ref_tree);

   //call this funtion to get the text for the service startTask
    std::string getText();

public Q_SLOTS:

    //this funtion will set the widgets for the task selected
    void controlWidget(const QString &);

    void rotate_callback_1();

    void rotate_callback_2();

private:

    QGridLayout * grid;

    QVBoxLayout * vertical_layout;

    QFrame * top_line;

    QFrame * bot_line;

    QHBoxLayout * top_line_layout;

    QHBoxLayout * bot_line_layout;

    bool first_iter;
    //YOU MUST HAVE DONE INIT before doing any operation
    TreeFileManager * tfm;
    //storing the text
    std::string text;
    //map betwen the line_edit and its label_text
    std::map<QLineEdit * ,std::string> lineEdit_toLabel;

    bool addWidgets(const std::vector<std::string> & ,const std::vector<std::string> &);

    void deleteWidgets(QLayout* layout);

    std::string procces_params(std::vector<std::string>);


    /* this function is only called when the task is rotate */
    void rotate_special_function();



};
#endif // MyCustomWidget_H
